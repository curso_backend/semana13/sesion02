from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import CrearAutor, EditarAutor, EliminarAutor, ListarAutores, listar_libros

urlpatterns = [
    path('listar_autores/', login_required(ListarAutores.as_view()), name='listar_autores'),
    # path('autor/<int:id>', listar_autores, name='listar_autores'),
    path('crear_autor/', CrearAutor.as_view(), name='crear_autor'),
    path('editar_autor/<int:pk>', EditarAutor.as_view(), name='editar_autor'),
    path('eliminar_autor/<int:pk>', EliminarAutor.as_view(), name='eliminar_autor'),

    path('listar_libros/', login_required(listar_libros), name='listar_libros')
]
