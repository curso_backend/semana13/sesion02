# Generated by Django 2.2.6 on 2019-11-02 23:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libro', '0005_libro_imagen'),
    ]

    operations = [
        migrations.AlterField(
            model_name='libro',
            name='imagen',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='pictures/libro'),
        ),
    ]
