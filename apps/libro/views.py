from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from .forms import AutorForm
from .models import Autor, Libro
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


class Home(TemplateView):
    template_name = 'libro/index.html'

    def get_context_data(self, *args, **kwargs):
        libros = Libro.objects.prefetch_related("autor").all()
        context = super(Home, self).get_context_data(*args, **kwargs)
        context['libros'] = libros
        return context


class ListarAutores(ListView):
    model = Autor
    template_name = 'libro/autor/listar_autor.html'
    queryset = Autor.objects.all()
    context_object_name = 'autores'


class CrearAutor(CreateView):
    model = Autor
    form_class = AutorForm
    template_name = 'libro/autor/crear_autor.html'
    success_url = reverse_lazy('libro:listar_autores')


class EditarAutor(UpdateView):
    model = Autor
    form_class = AutorForm
    template_name = 'libro/autor/editar_autor.html'
    success_url = reverse_lazy('libro:listar_autores')


class EliminarAutor(DeleteView):
    model = Autor
    form_class = AutorForm
    template_name = 'libro/autor/eliminar_autor.html'
    success_url = reverse_lazy('libro:listar_autores')

    def get_context_data(self, *args, **kwargs):
        autor = Autor.objects.get(id=self.kwargs.get('pk'))
        context = super(EliminarAutor, self).get_context_data(*args, **kwargs)
        context['autor'] = autor
        return context


@require_http_methods(['GET'])
def listar_libros(request):
    libros = Libro.objects.prefetch_related("autor").all()
    return render(request, 'libro/libro/listar_libro.html', {
        'libros': libros
    })
